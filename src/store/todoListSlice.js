import { createSlice } from '@reduxjs/toolkit'
import { updateTodoList } from '../apis/todos'

export const todoListSlice = createSlice({
    name: 'todoListSlice',
    initialState: {
        todoList: [],
    },
    reducers: {
        setTodoList: (state, { payload }) => {
            state.todoList = payload
        },

        addTodoItem: (state, { payload }) => {
            state.todoList.push({
                id: Math.ceil(Math.random() * 1000000),
                text: payload.text.text,
                done: false,
            })
        },
        doneTodoItem: (state, { payload }) => {
            const tmpItem = state.todoList.find(item => item.id === payload)
            tmpItem.done = !tmpItem.done
            console.log(tmpItem)
            updateTodoList(tmpItem.id, tmpItem.text, tmpItem.done)
        },
        deleteTodoItem: (state, { payload }) => {

            state.todoList = state.todoList.filter(
                (item, index) => index !== payload,
            )
        },
    },
})

// Action creators are generated for each case reducer function
export const { addTodoItem, doneTodoItem, deleteTodoItem, setTodoList } =
    todoListSlice.actions

export default todoListSlice.reducer
