import { useSelector } from 'react-redux'
import TodoItem from './TodoItem/index.jsx'
import { useEffect } from 'react'
import useTodos from '../hooks/useTodos.jsx'

const TodoGroup = () => {

    const { getTodoList } = useTodos()
    
    useEffect(() => {
        getTodoList()
    },[])
    
    const todoList = useSelector((state) => state.todoList.todoList)
  

    return (
        <div className="todoGroup">
            {todoList.map((item) => {
                return (
                    <TodoItem
                        value={item}
                        key={item.id}
                    ></TodoItem>
                )
            })}
        </div>
    )
}
export default TodoGroup
