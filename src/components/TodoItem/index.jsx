import './index.css'
import { useDispatch } from 'react-redux'
import { doneTodoItem } from '../../store/todoListSlice'
import useTodos from '../../hooks/useTodos'
import {  Button, Modal, Input } from 'antd'
import { useState } from 'react'

const TodoItem = ({ value }) => {

    const { deleteTodoList, updateTodoList } = useTodos()
    const dispatch = useDispatch()


    const [editModalVisible, setEditModalVisible] = useState(false)
    const [editedTodo, setEditedTodo] = useState(null)
    const [editedText, setEditedText] = useState('')

    const handleClick = () => {
        dispatch(doneTodoItem(value.id))
    }
    const handleDelete = () => {
        deleteTodoList(value.id)
    }

    const handleEditModalOpen = (todo) => {
        setEditedTodo(todo)
        setEditedText(todo.text)
        setEditModalVisible(true)
    }

    const handleEditModalClose = () => {
        setEditModalVisible(false)
    }
    const handleEditSave = async () => {
        if (editedText && editedText.trim().length > 0) {
            updateTodoList(editedTodo.id, editedText)
            setEditModalVisible(false)
        }

    }

    return (
        <div className="todo-item-container">
            <div className="todo-item" onClick={handleClick}>
                <div
                    className={
                        value.done ? 'todo-item-text line-through' : 'todo-item-text'
                    }
                >
                    {value.text}
                </div>
            </div>
            <button className="todo-item-close" onClick={handleDelete}>
                X
            </button>
            <button className="todo-item-close" onClick={() => handleEditModalOpen(value)}>
                edit
            </button>

            <Modal
                title="Edit Todo"
                visible={editModalVisible}
                onCancel={handleEditModalClose}
                onOk={handleEditSave}
            >
                <Input
                    type="text"
                    value={editedText}
                    onChange={(e) => setEditedText(e.target.value)}
                />
            </Modal>
            <>


            </>


        </div>
    )
}
export default TodoItem