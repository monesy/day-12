import {  useState } from 'react'
import './index.css'
import useTodos from '../../hooks/useTodos'

const TodoGenerator = () => {
    const [inputValue, setInputValue] = useState('')
    const { addTodoList } = useTodos()

    const onClick = () => {
        if (inputValue.length === 0) {
            alert('请输入内容')
            return
        }
        addTodoList(inputValue)
        setInputValue('')
    }

    const onChange = (e) => {
        setInputValue(e.target.value)
    }

    return (
        <div className="todoGenerator">
            <input
                type="text"
                value={inputValue}
                onChange={onChange}
                placeholder="请输入Todo"
            ></input>
            <button onClick={onClick}>add</button>
        </div>
    )
}

export default TodoGenerator
