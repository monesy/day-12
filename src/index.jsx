import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import { Provider } from 'react-redux'
import store from './store/store'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import DoneList from './Pages/DoneList'
import Help from './Pages/Help'
import TodoList from './components/TodoList'
import DoneDetails from './Pages/DoneDetails'
import NotFound from './Pages/NotFound'

const root = ReactDOM.createRoot(document.getElementById('root'))
const router = createBrowserRouter([
    {
        path: "/",
        element: <App></App>,
        children: [
            {
                index: true,
                element: <TodoList></TodoList>
            },
            {
                path: "/done",
                element: <DoneList></DoneList>
            },
            {
                path: "/help",
                element: <Help></Help>
            },
            {
                path: "/done/:id",
                element: <DoneDetails></DoneDetails>
            },
        ]
    },
    {
        path:"*",
        element:<NotFound></NotFound>

    }

])


root.render(
    <Provider store={store}>
        <RouterProvider router={router} />
    </Provider>,
)
