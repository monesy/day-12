import './App.css'
import TodoList from './components/TodoList'
import {  Outlet } from 'react-router-dom'
import Nevigate from './Pages/Nevigate'

function App() {
    return (
        <div className="App">
            <Nevigate></Nevigate>
            <Outlet></Outlet>
        </div>
    )
}

export default App
