import axios from "axios";

const instance = axios.create ({
    baseURL: 'https://64c0b66b0d8e251fd11264a7.mockapi.io/api/',
});

export default instance
