import api from "./api";


export const getTodoList = async () => {
    return api.get('/todos')
}

export const addTodoList = async (text) => {
    return api.post('/todos', {
        text,
        done: false
    })
}

export const deleteTodoList = async (id) => {
    return api.delete(`/todos/${id}`)
}

export const updateTodoList = async (id, text, done) => {
    return api.put(`/todos/${id}`, {
        text,
        done
    })
}



const todoApi = {
    getTodoList,
    addTodoList,
    deleteTodoList,
    updateTodoList,

}
export default todoApi


