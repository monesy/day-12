import { useDispatch } from "react-redux"
import { setTodoList } from "../store/todoListSlice"
import todoApi from "../apis/todos"

const useTodos = () => {
    const dispatch = useDispatch()
    const getTodoList= () => {
        todoApi.getTodoList().then( response => {
        dispatch(setTodoList(response.data))
    }) 
    }

    const addTodoList= (text) => {
        todoApi.addTodoList(text).then( response => {
        getTodoList()
    }) 
    }
    const deleteTodoList= (id) => {
        todoApi.deleteTodoList(id).then( response => {
        getTodoList()
    }) 
    }
    const updateTodoList= (id,text) => {
        todoApi.updateTodoList(id,text).then( response => {
        getTodoList()
    }) 
    }
    const updateTodoListStatus= (id,done) => {
        todoApi.updateTodoListStatus(id,done).then( response => {
        getTodoList()
    }) 
    }

    

return {
    getTodoList,
    addTodoList,
    deleteTodoList,
    updateTodoList,
    updateTodoListStatus
}
    
}
export default useTodos

