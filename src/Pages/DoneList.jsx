import { useEffect } from "react"
import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"


const DoneList = () => {
    const doneList = useSelector(state => state.todoList.todoList).filter(todoItem => todoItem.done)
    const navigate = useNavigate()
    const handleClick = (id) => {
        navigate(`/done/${id}`)
    }


    return (
        <div >
            {
                doneList.map((item) =>
                    <div onClick={() => handleClick(item.id)}><input value={item.text} disabled /></div>
                )
            }

        </div>
    )
}

export default DoneList