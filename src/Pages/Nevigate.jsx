import { Link } from "react-router-dom"

const Nevigate = () => {
    return (
        <div>
            <ul>
                <li>
                    <Link to={'/'} >home</Link>
                </li>
                <li>
                    <Link to={'/help'} >help</Link>
                </li>
                <li>
                    <Link to={'/done'} >done</Link>
                </li>

            </ul>

        </div>
    )
}
export default Nevigate