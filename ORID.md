**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview：我的作业没考虑到一些边界值的处理
2. React Redux：学习了React Redux的概念，老师用一个公司以及公司内资源管理部门讲述，因为我之前学过vuex，因此这块知识我有一点基础，但是在实现时还是有时会被卡住，还得经常练习
3. Redux实践：redux的主要的过程就是用dispatch调用方法对state里的value进行操作，在需要用到state中的数据时就用useSelector进行取值
4. React test：用@testing-library/react中的api模拟DOM事件，并进行验证操作
5. flex布局：老师给了我们一个网页去练习flex中的布局，复习了flex布局中常用的css如justify-content，align-items

**R (Reflective): Please use one word to express your feelings about today's class.**

练习

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

redux的实践比较简单易懂，能快速帮助我们上手redux进行状态管理

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

自己对于react的实践还不够多，需要多加练习

