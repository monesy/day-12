### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	今天上午进行了Code Review，然后进行了React router的学习，然后学习了使用Mock API去模拟模拟生成数据并用Postman进行尝试操作。学习了使用hooks 去将api聚合在一起，使代码简洁。学习了UI Library和ant-design的基本用法。

### R (Reflective): Please use one word to express your feelings about today's class.

​	充实

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	相比昨天来说对前端代码熟悉程度提升了，但今天犯了一些低级错误，传输了错误数据导致代码异常导致浪费了很多时间甚至拖累了我的课程进度。以后一定不能乱来，要

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	以后一定三思而后行，不能乱来，在前端也要做到小步提交，遇到问题若长时间无法解决就举手找老师。
